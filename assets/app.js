(function () {
  "use strict";
  var app = angular.module("pokeSniper", []);

  app.config(["$compileProvider", function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|pokesniper2):/);
  }]);

  app.service("Pokemon", [function () {
    return function Pokemon(pokemonData) {
      var self = this;
      var defaults = {
        name: null,
        slug: '',
        isWanted: false
      };
      angular.extend(this, defaults, pokemonData);

      this.image = function () {
        var name = self.name.toLowerCase();
        switch (name) {
          case "farfetch'd": name = "farfetchd"; break;
          case 'nidoranmale': name = "nidoran-f"; break;
          case 'nidoranfemale': name = "nidoran-m"; break;
          case 'mr. mime': name = "mr-mime"; break;
        }

        return "/assets/images/" + name + ".jpg";
      };

      this.iWant = function () {
        self.isWanted = true;
      };

      this.iDontWant = function () {
        self.isWanted = false;
      };
    };
  }]);

  app.service("PokemonSpawn", ["$http", function ($http) {
    return function PokemonSpawn(spawnData, pokemon) {
      var self = this;
      var defaults = {
        pokemon: pokemon,
        coords: '',
        iv: '0',
        time: new moment(),
        unixtime: new moment().unix(),
        isPerfect: false,
        alreadSniped: false
      };
      angular.extend(this, defaults, spawnData);

      if (self.iv == 100) {
        self.isPerfect = true;
      }

      if (self.iv > 100) {
        self.iv = self.iv.slice(0, 2);
      }

      if (self.pokemon.isWanted) {
        new Audio('/assets/sounds/beep.mp3').play();
      }
    };
  }]);

  app.service("Pokemons", ["Pokemon", "PokemonSpawn", function (Pokemon, PokemonSpawn) {
    return function Pokemons() {
      var self = this;
      this.spawns = [];
      this.list = [];

      angular.forEach(_pokemon_list, function (name) {
        self.list.push(new Pokemon({'name': name}));
      });

      this.include = function (pokemonData) {
        var exists = self.spawns.filter(function (spawn) {
          return spawn.pokemon.name == pokemonData.name && spawn.coords == pokemonData.coords;
        });

        if (exists.length == 0) {
          var pokemon = self.getPokemon(pokemonData.name);
          delete pokemonData["name"];
          this.spawns.push(new PokemonSpawn(pokemonData, pokemon));
        }
      };

      this.getPokemon = function (name) {
        return this.list.filter(function (pokemon) {
          return pokemon.name == name;
        })[0];
      };

      this.checkExpires = function () {
        var now = new moment().unix();
        var newSpawnList = [];
        angular.forEach(self.spawns, function (spawn) {
          if (now - spawn.unixtime < _expiresAfter) {
            newSpawnList.push(spawn);
          }
        });
        self.spawns = newSpawnList;
      };
    };
  }]);

  app.controller("PokeSniper", ["$scope", "$http", "$timeout", "$interval", "Pokemons", "Pokemon", function ($scope, $http, $timeout, $interval, Pokemons, Pokemon) {
    $scope.pokemons = new Pokemons();

    $scope.parseResponse = function (response) {
      angular.forEach(response, function (pokemonData) {
        $scope.pokemons.include(pokemonData);
      });
    };

    $scope.fetch = function () {
      $http.get("/fetch").then(function (response) {
        $scope.parseResponse(response.data);
        $timeout(function () {
          $scope.fetch();
        }, 3000);
      });
    };

    $interval(function () {
      $scope.pokemons.checkExpires();
    }, 10000);

    $scope.fetch();
  }]);
})();