var express = require('express');
var request = require('request');
var app = express();

app.use("/vendor", express.static('bower_components'));
app.use("/assets", express.static('assets'));
app.engine('html', require('ejs').renderFile);

app.get('/', function (req, res) {
  res.render('app.html');
});

app.get('/fetch', function (req, res) {
  request.get({
    url: 'http://pokesniper.org/newapiman.txt',
    headers: {
      'Host': 'pokesniper.org',
      'Connection': 'keep-alive',
      'Referer': 'http://pokesniper.org/',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
    }
  },
  function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.send(body);
    }
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});